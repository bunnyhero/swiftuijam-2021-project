//
//  NextStatDiceOverlayView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-07.
//

import SwiftUI

private struct DieTransform {
    let position: CGPoint
    let rotation: Angle
    let opacity: Double
}

struct NextStatDiceOverlayView: View {
    let rollState: NextStatRollerView.RollState
    let dice: [DieValue]
    let resultBounds: CGRect
    
    @State private var randomStarts: [CGPoint] = randomStartPositions()
    @State private var randomEnds: [CGPoint] = randomEndPositions()
    @State private var randomRotations: [Double] = randomRotations()
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                ForEach(dice.indices) { index in
                    let transform = transform(for: index, size: geometry.size, resultBounds: resultBounds)
                    D6View(pips: dice[index].value)
                        .frame(width: 44, height: 44)
                        .rotationEffect(transform.rotation)
                        .position(transform.position)
                        .opacity(transform.opacity)
                }
            }
            .animation(.easeOut(duration: 1), value: rollState)
        }
    }
    
    static func randomStartPositions() -> [CGPoint] {
        // These are relative to height/width, so from 0...1.0
        // Either the left or right edge, and any y-value
        
        // Make as many as we might need. Say ... 4
        return (0..<4).map({ _ in
            CGPoint(
                x: CGFloat.random(in: 0..<1.0) < 0.5 ? 0 : 1,
                y: CGFloat.random(in: 0...1.0)
            )
        })
    }

    static func randomEndPositions() -> [CGPoint] {
        // These are relative to height/width, so from 0...1.0
        // Either the left or right edge, and any y-value
        
        // Make as many as we might need. Say ... 4
        return (0..<4).map({ _ in
            CGPoint(
                x: CGFloat.random(in: 0.25..<0.75),
                y: CGFloat.random(in: 0.25..<0.75)
            )
        })
    }
    
    static func randomRotations() -> [Double] {
        // Make as many as we might need. Say ... 4
        return (0..<4).map({ _ in
            Double.random(in: 1..<3) * 360
        })
    }

    private func transform(for dieIndex: Int, size: CGSize, resultBounds: CGRect) -> DieTransform {
        switch rollState {
        case .idle:
            return DieTransform(
                position: CGPoint(
                    x: randomStarts[dieIndex].x * size.width,
                    y: randomStarts[dieIndex].y * size.height
                ),
                rotation: .degrees(0),
                opacity: 0
            )
        
        case .rolling:
            return DieTransform(
                position: CGPoint(
                    x: randomEnds[dieIndex].x * size.width,
                    y: randomEnds[dieIndex].y * size.height
                ),
                rotation: .degrees(randomRotations[dieIndex]),
                opacity: 1
            )
        
        default:
            return DieTransform(
                position: CGPoint(
                    x: resultBounds.midX,
                    y: resultBounds.midY
                ),
                rotation: .degrees(randomRotations[dieIndex]),
                opacity: 0
            )
        }
    }
}

struct NextStatDiceOverlayView_Previews: PreviewProvider {
    static let dice = [
        DieValue(value: 3), DieValue(value: 5), DieValue(value: 1)
    ]
    static let resultBounds = CGRect(x: 100, y: 100, width: 50, height: 50)
    static var previews: some View {
        NextStatDiceOverlayView(rollState: .idle, dice: dice, resultBounds: resultBounds)
        NextStatDiceOverlayView(rollState: .rolling, dice: dice, resultBounds: resultBounds)
        NextStatDiceOverlayView(rollState: .result, dice: dice, resultBounds: resultBounds)
    }
}
