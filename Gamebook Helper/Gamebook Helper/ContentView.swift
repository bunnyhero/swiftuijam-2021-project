//
//  ContentView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-05.
//

import SwiftUI

struct ContentView: View {
    @State private var stats = [
        Stat(name: "Skill", initialValue: 12, currentValue: 10),
        Stat(name: "Stamina", initialValue: 12, currentValue: 12),
        Stat(name: "Luck", initialValue: 9, currentValue: 9)
    ]
    @State private var notes = "Your notes go here"
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Spacer()
                    Text("Initial")
                        .font(.subheadline)
                }
                ForEach($stats) { $stat in
                    StatView(name: stat.name, initialValue: $stat.initialValue, currentValue: $stat.currentValue)
                }
                SampleDieRollerView()
                    .frame(minHeight: 44)
                    .fixedSize(horizontal: false, vertical: true)
                Text("Notes")
                TextEditor(text: $notes)
                    .border(Color.red, width: 1)
            }
            .padding()
            .navigationTitle("Adventure Sheet")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    EditButton()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
