//
//  MultiBoundsPreferenceKey.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-07.
//

import SwiftUI

struct MultiBoundsPreferenceKey: PreferenceKey {
    static var defaultValue: [String: Anchor<CGRect>] = [:]
    
    static func reduce(
        value: inout [String : Anchor<CGRect>],
        nextValue: () -> [String : Anchor<CGRect>]
    ) {
        value.merge(nextValue(), uniquingKeysWith: { $1 })
    }
}
