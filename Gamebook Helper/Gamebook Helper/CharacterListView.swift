//
//  CharacterListView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-07.
//

import SwiftUI

struct CharacterListView: View {
    @Binding var characterSheets: [CharacterSheet]
    @State private var isShowingWizard = false
    
    var body: some View {
        List {
            ForEach(characterSheets) { charSheet in
                VStack {
                    HStack {
                        Text(charSheet.name.isEmpty ? "(unnamed)" : charSheet.name)
                            .font(.handwritten(.medium))
                        Spacer()
                    }
                    HStack {
                        Text(charSheet.stats.map({ "\($0.name): \($0.initialValue)" }).joined(separator: ", "))
                            .font(.typewritten(.small))
                        Spacer()
                    }
                }
            }
            Button(action: { isShowingWizard.toggle() }) {
                Label("Roll new character", systemImage: "dice")
            }
        }
        .navigationTitle("Characters")
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: { isShowingWizard.toggle() }) {
                    Label("Add", systemImage: "plus")
                }
            }
        }
        .sheet(isPresented: $isShowingWizard, onDismiss: nil) {
            NavigationView {
                SheetWizardView(completion: { charSheet in
                    characterSheets.append(charSheet)
                    isShowingWizard = false
                })
            }
        }
    }
}

struct CharacterListView_Previews: PreviewProvider {
    @State private static var characterSheets: [CharacterSheet] =
        (0..<1).map { _ in CharacterSheet.generateTestSheet() }
    static var previews: some View {
        NavigationView {
            CharacterListView(characterSheets: $characterSheets)
        }
    }
}
