//
//  GeometryTestView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import SwiftUI


struct GeometryTestView: View {
    @State private var name = ""
    var body: some View {
        VStack {
            TextField("florp", text: $name)
            Spacer(minLength: 300)
            HStack {
                Text("cheese")
                    .anchorPreference(
                        key: MultiBoundsPreferenceKey.self, value: .bounds, transform: { ["cheese": $0] }
                    )
                Text("blorg")
                    .anchorPreference(
                        key: MultiBoundsPreferenceKey.self, value: .bounds, transform: { ["blorg": $0] }
                    )
            }
        }
        .overlayPreferenceValue(MultiBoundsPreferenceKey.self) { anchorMap in
            GeometryReader { geometry in
                if let anchor = anchorMap["cheese"] {
                    Rectangle()
                        .fill(Color.blue)
                        .opacity(0.4)
                        .frame(width: geometry[anchor].width, height: geometry[anchor].height)
                        .offset(x: geometry[anchor].minX, y: geometry[anchor].minY)
                }
            }
        }
    }
}

struct GeometryTestView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryTestView()
    }
}
