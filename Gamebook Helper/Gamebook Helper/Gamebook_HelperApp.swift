//
//  Gamebook_HelperApp.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-05.
//

import SwiftUI

@main
struct Gamebook_HelperApp: App {
    @State private var characterSheets: [CharacterSheet] = []
    var body: some Scene {
        WindowGroup {
//            ContentView()
            NavigationView {
                CharacterListView(characterSheets: $characterSheets)
            }
        }
    }
}
