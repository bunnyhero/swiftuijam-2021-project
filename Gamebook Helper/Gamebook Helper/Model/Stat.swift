//
//  Stat.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-05.
//

import Foundation

struct Stat: Identifiable, Equatable {
    var name: String
    var initialValue: Int
    var currentValue: Int

    var id: String { return name }
}

struct StatMaker {
    let name: String
    let base: Int
    let diceCount: Int
    
    func makeStat(withRoll roll: Int) -> Stat {
        return Stat(name: name, initialValue: base + roll, currentValue: base + roll)
    }

    func makeStat(withTotal total: Int) -> Stat {
        return Stat(name: name, initialValue: total, currentValue: total)
    }
}
