//
//  DieValue.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import Foundation

struct DieValue: Identifiable, Equatable {
    var id: UUID = UUID()
    var value: Int
    
    mutating func roll() {
        self.value = Int.random(in: 1...6)
    }
}
