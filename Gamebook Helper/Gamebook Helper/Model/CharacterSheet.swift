//
//  CharacterSheet.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import Foundation

struct CharacterSheet: Equatable, Identifiable {
    static let statMakers: [StatMaker] = [
        StatMaker(name: "Skill", base: 6, diceCount: 1),
        StatMaker(name: "Stamina", base: 12, diceCount: 2),
        StatMaker(name: "Luck", base: 6, diceCount: 1),
    ]
    var name: String = ""
    var stats: [Stat] = [
//        Stat(name: "Skill", initialValue: 8, currentValue: 8)
    ]
    var id: UUID = UUID()
    
    static func generateTestSheet() -> CharacterSheet {
        var sheet = CharacterSheet()
        sheet.stats = statMakers.map({ statMaker in
            let roll = (0..<statMaker.diceCount).reduce(0, { accum, _ in accum + Int.random(in: 1...6) })
            return statMaker.makeStat(withRoll: roll)
        })
        return sheet
    }
}
