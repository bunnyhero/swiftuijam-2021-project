//
//  NextStatRollerView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import SwiftUI

struct NextStatRollerView: View {
    let nextStat: StatMaker
    let completion: ((Int) -> Void)?
    
    enum RollState {
        case idle
        case rolling
        case result
        case total
    }
    
    @State private var rollState: RollState = .idle
    @State private var dice: [DieValue] = [
        DieValue(value: Int.random(in: 1..<6)),
        DieValue(value: Int.random(in: 1..<6)),
        DieValue(value: Int.random(in: 1..<6)),
    ]

    var body: some View {
        VStack(spacing: 12) {
            NextStatLabelView(nextStat: nextStat, rollState: rollState, total: total())
            Text("Roll \(nextStat.diceCount)d6 and add \(nextStat.base) for \(nextStat.name)")
                .font(.typewritten(.medium))
            HStack {
                Spacer()
                Button("Roll") {
                    roll()
                }
                .buttonStyle(.borderedProminent)
                .disabled(rollState != .idle)
                Spacer()
            }
        }
        .overlayPreferenceValue(MultiBoundsPreferenceKey.self) { anchorMap in
            GeometryReader { geometry in
                if let anchor = anchorMap["question"] {
                    NextStatDiceOverlayView(
                        rollState: rollState,
                        dice: Array(dice[0..<nextStat.diceCount]),
                        resultBounds: geometry[anchor]
                    )
                }
            }
        }
    }
    
    private func roll() {
        guard rollState == .idle else { return }
        rollState = .rolling
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            rollState = .result
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                rollState = .total
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    completion?(total())
                }
            }
        }
    }
    
    private func total() -> Int {
        nextStat.base + dice[0..<nextStat.diceCount].reduce(0, { $0 + $1.value })
    }
}


private struct NextStatLabelView: View {
    let nextStat: StatMaker
    let rollState: NextStatRollerView.RollState
    let total: Int
    
    var body: some View {
        let isTotal = rollState == .total
        let fillColour: Color = isTotal ? .clear : .init(uiColor: .systemBackground)

        HStack {
            Text(nextStat.name)
            Spacer()
                .frame(maxWidth: isTotal ? .infinity : 3)
            Text(isTotal ? "\(total)" : "= ?")
                .anchorPreference(key: MultiBoundsPreferenceKey.self, value: .bounds) { anchor in
                    ["question": anchor]
                }
        }
        .font(.handwritten(.large))
        .padding()
        .background {
            RoundedRectangle(cornerRadius: 12)
                .fill(fillColour)
                .shadow(radius: 8, x: 8, y: 8)
        }
        .rotationEffect(.degrees(isTotal ? 0 : -4))
        .animation(.default, value: rollState)
    }
}


struct NextStatRollerView_Previews: PreviewProvider {
    static var previews: some View {
        NextStatRollerView(nextStat: StatMaker(name: "Skill", base: 6, diceCount: 1), completion: nil)
    }
}
