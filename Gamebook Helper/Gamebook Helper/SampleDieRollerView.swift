//
//  SampleDieRollerView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import SwiftUI

struct SampleDieRollerView: View {
    @State private var dice: [DieValue] = [
        DieValue(value: 4),
        DieValue(value: 5),
    ]
    @State private var rollCount: Int = 0
    
    var body: some View {
        VStack {
            HStack {
                Spacer().layoutPriority(0.25)
                ForEach(dice) { die in
                    D6View(pips: die.value)
                        .rotationEffect(.degrees(Double(rollCount * 180)))
                        .animation(.default, value: rollCount)
                        .layoutPriority(0.5)
                    Spacer().layoutPriority(0.25)
                }
                Spacer().layoutPriority(0.25)
                Text("= \(dice.reduce(0, { $0 + $1.value }))")
                    .font(.title)
                    .animation(.default, value: rollCount)
                    .layoutPriority(0.5)
                Spacer().layoutPriority(0.25)
            }
            .frame(minHeight: 44)
            .fixedSize(horizontal: false, vertical: true)
            Button("Roll") {
                for i in 0..<dice.count {
                    dice[i].value = Int.random(in: 1...6)
                }
                rollCount += 1
            }
        }
    }
}

struct SampleDieRoller_Previews: PreviewProvider {
    static var previews: some View {
        SampleDieRollerView()
            .previewLayout(.sizeThatFits)
    }
}
