//
//  StatView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-05.
//

import SwiftUI

struct StatView: View {
    @Environment(\.editMode) var editMode
    let name: String
    @Binding var initialValue: Int
    @Binding var currentValue: Int
    
    private var isEditing: Bool { editMode?.wrappedValue == .active }

    var body: some View {
        HStack {
            Text(name)
                .font(.title)
            Spacer()
            if !isEditing {
                Stepper(value: $currentValue, in: 0...initialValue) {
                    Text("\(currentValue)")
                        .font(.title)
                }
                .fixedSize()
                Text("\(initialValue)")
                    .font(.callout)
                    .opacity(0.5)
            } else {
                VStack {
                    TextField("\(initialValue)", value: $initialValue, format: IntegerFormatStyle())
                        .font(.headline)
                        .keyboardType(.numberPad)
                        .multilineTextAlignment(.trailing)
                        .fixedSize(horizontal: false, vertical: true)
                    Divider()
                }
            }
        }
        .onChange(of: isEditing) { newValue in
            if newValue == false {
                if currentValue > initialValue {
                    currentValue = initialValue
                }
            }
        }
    }
    
}

struct StatView_Previews: PreviewProvider {
    @State static var currentValue: Int = 8
    
    static var previews: some View {
        StatView(name: "Skill", initialValue: Binding.constant(12), currentValue: $currentValue)
            .previewLayout(.sizeThatFits)
            .environment(\.editMode, Binding.constant(EditMode.active))
        StatView(name: "Skill", initialValue: Binding.constant(10), currentValue: $currentValue)
            .previewLayout(.sizeThatFits)
            .environment(\.editMode, Binding.constant(EditMode.inactive))
    }
}
