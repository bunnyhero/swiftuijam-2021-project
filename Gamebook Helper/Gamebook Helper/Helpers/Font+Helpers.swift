//
//  Font+Helpers.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import SwiftUI

enum FontSize: Int {
    case small
    case medium
    case large
}

extension Font {
    static func handwritten(_ size: FontSize) -> Font {
        switch size {
        case .small:
            return self.custom("Marker Felt", size: 16)
        case .medium:
            return self.custom("Marker Felt", size: 24)
        case .large:
            return self.custom("Marker Felt", size: 32)
        }
    }

    static func typewritten(_ size: FontSize) -> Font {
        let sizes: [FontSize: CGFloat] = [
            .small: 16,
            .medium: 24,
            .large: 32
        ]
        return custom("American Typewriter Condensed", size: sizes[size]!)
    }
}
