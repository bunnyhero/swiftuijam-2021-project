//
//  GeometryProxy+Helpers.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import SwiftUI

extension GeometryProxy {
    var minLength: CGFloat { min(size.width, size.height) }
}
