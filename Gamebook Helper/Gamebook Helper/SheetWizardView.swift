//
//  SheetWizardView.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import SwiftUI

struct SheetWizardView: View {
    @State private var characterSheet = CharacterSheet()
    let completion: (CharacterSheet) -> Void
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(characterSheet.stats) { stat in
                    FinishedStatView(stat: stat)
                }
                if characterSheet.stats.count < CharacterSheet.statMakers.count {
                    let nextStat = CharacterSheet.statMakers[characterSheet.stats.count]
                    NextStatRollerView(nextStat: nextStat) { total in
                        let stat = nextStat.makeStat(withTotal: total)
                        characterSheet.stats.append(stat)
                    }
                    .id(characterSheet.stats.count) // Explicit ID so that each new stat gets reset
                } else {
                    TextField("Name", text: $characterSheet.name, prompt: Text("Name your character"))
                        .textFieldStyle(.roundedBorder)
                        .font(.handwritten(.medium))
                        .padding()
                    HStack {
                        Button("Reroll") {
                            characterSheet = CharacterSheet()
                        }
                        .buttonStyle(.bordered)
                        Button("Keep Character") {
                            completion(characterSheet)
                        }
                        .buttonStyle(.borderedProminent)
                    }
                }
                Spacer()
            }
            .animation(.default, value: characterSheet.stats.count)
        }
        .navigationTitle("New character")
        .navigationBarTitleDisplayMode(.inline)
    }
}

private struct FinishedStatView: View {
    let stat: Stat
    var body: some View {
        HStack {
            Text(stat.name)
            Spacer()
            Text("\(stat.initialValue)")
        }
        .font(.handwritten(.large))
        .padding()
    }
}



struct SheetWizardView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SheetWizardView { characterSheet in
                print(characterSheet.stats)
            }
        }
    }
}
