//
//  D6View.swift
//  Gamebook Helper
//
//  Created by bunnyhero on 2021-11-06.
//

import SwiftUI

struct D6View: View {
    let pips: Int
    let colour: Color
    let pipColour: Color
    
    init(pips: Int, colour: Color = .red, pipColour: Color = .white) {
        self.pips = pips
        self.colour = colour
        self.pipColour = pipColour
    }
    
    private let arrangements: [[Int]] = [
        /* 0 */ [], // Just so we don't have to muck with the inded
        /* 1 */ [4],
        /* 2 */ [0, 8],
        /* 3 */ [0, 4, 8],
        /* 4 */ [0, 2, 6, 8],
        /* 5 */ [0, 2, 4, 6, 8],
        /* 6 */ [0, 2, 3, 5, 6, 8],
    ]
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                let offset = CGSize(
                    width: (geometry.size.width - geometry.minLength) / 2,
                    height: (geometry.size.height - geometry.minLength) / 2
                )
                RoundedRectangle(cornerRadius: geometry.minLength / 6)
                    .fill(colour)
                .aspectRatio(1, contentMode: .fit)
                .offset(offset)
                ForEach(0..<pips, id: \.self) { pipIndex in
                    Circle()
                        .fill(pipColour)
                        .offset(
                            x: geometry.minLength * position(for: arrangements[pips][pipIndex]).x,
                            y: geometry.minLength * position(for: arrangements[pips][pipIndex]).y
                        )
                        .offset(offset)
                        .frame(width: geometry.minLength / 6, height: geometry.minLength / 6, alignment: .center)
                }
            }
        }
        .aspectRatio(1, contentMode: .fit)
    }

    private func position(for placeIndex: Int) -> CGPoint {
        CGPoint(
            x: -0.25 + Double(placeIndex % 3) * 0.25,
            y: -0.25 + Double(placeIndex / 3) * 0.25
        )
    }
}

struct D6View_Previews: PreviewProvider {
    static var previews: some View {
        D6View(pips: 2, colour: Color.yellow, pipColour: Color.green)
            .previewLayout(.sizeThatFits)
        D6View(pips: 6)
            .previewLayout(.sizeThatFits)
    }
}
